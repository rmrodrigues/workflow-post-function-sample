package ut.com.xpandit.openday;

import org.junit.Test;
import com.xpandit.openday.api.MyPluginComponent;
import com.xpandit.openday.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}