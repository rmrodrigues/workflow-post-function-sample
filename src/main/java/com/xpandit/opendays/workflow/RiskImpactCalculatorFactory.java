package com.xpandit.opendays.workflow;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

public class RiskImpactCalculatorFactory implements WorkflowPluginFunctionFactory{

    public RiskImpactCalculatorFactory() {
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> arg0) {
        return new HashMap<String, String>();
    }

    @Override
    public Map<String, ?> getVelocityParams(String arg0, AbstractDescriptor arg1) {
        return new HashMap<String, String>();
    }

}
