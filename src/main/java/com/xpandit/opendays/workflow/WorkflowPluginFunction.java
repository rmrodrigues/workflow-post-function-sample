package com.xpandit.opendays.workflow;

import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

/**
 * The Class WorkflowPluginFunction.
 */
public class WorkflowPluginFunction extends AbstractJiraFunctionProvider {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(WorkflowPluginFunction.class);

    /** The Constant COMPLEXITY_FIELD_NAME. */
    private static final String COMPLEXITY_FIELD_NAME = "Complexity";

    /** The Constant RISK_FACTOR_FIELD_NAME. */
    private static final String RISK_FACTOR_FIELD_NAME = "Risk Factor";

    private static final String RISK_IMPACT_FIELD_NAME = "Risk Impact";

    /** The Constant RISK_FACTOR_LOW. */
    private static final String RISK_FACTOR_LOW = "Low";

    /** The Constant RISK_FACTOR_MEDIUM. */
    private static final String RISK_FACTOR_MEDIUM = "Medium";

    /** The Constant RISK_FACTOR_HIGH. */
    private static final String RISK_FACTOR_HIGH = "High";

    /**
     * Instantiates a new workflow plugin function.
     */
    public WorkflowPluginFunction() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.workflow.FunctionProvider#execute(java.util.Map, java.util.Map,
     * com.opensymphony.module.propertyset.PropertySet)
     */
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("Start processing workflow post function");

        // Get Issue Object
        MutableIssue issue = getIssue(transientVars);

        if (issue != null) {
            logger.debug("Getting Complexity Custom Field");
            // Get Complexity Custom Field
            CustomField complexityCustomField = ComponentAccessor.getCustomFieldManager()
                    .getCustomFieldObjectByName(COMPLEXITY_FIELD_NAME);
            logger.debug("Getting Complexity value from Issue");
            // Get Complexity value from Issue
            Double complexityValue = (Double) issue.getCustomFieldValue(complexityCustomField);
            logger.debug("Get Risk Factor Custom Field");
            // Get Risk Factor Custom Field
            CustomField riskFactorCustomField = ComponentAccessor.getCustomFieldManager()
                    .getCustomFieldObjectByName(RISK_FACTOR_FIELD_NAME);
            logger.debug("Getting Risk Factor value from Issue");
            // Get Risk Factor value from Issue
            String riskFactorValue = ((Option) issue.getCustomFieldValue(riskFactorCustomField)).getValue();

            int riskFactorResolved = 1;
            if (RISK_FACTOR_LOW.equalsIgnoreCase(riskFactorValue)) {
                riskFactorResolved = 1;
            } else if (RISK_FACTOR_MEDIUM.equalsIgnoreCase(riskFactorValue)) {
                riskFactorResolved = 2;
            } else if (RISK_FACTOR_HIGH.equalsIgnoreCase(riskFactorValue)) {
                riskFactorResolved = 5;
            }
            logger.debug("riskFactorResolved: " + riskFactorResolved);
            Double riskImpactCalculatedValue = complexityValue * riskFactorResolved;
            logger.info("Risk Impact: " + riskImpactCalculatedValue);
            ModifiedValue val = new ModifiedValue(null, riskImpactCalculatedValue);
            logger.debug("Get Risk Impact Custom Field");
            // Get Risk Impact Custom Field
            CustomField riskImpactCustomField = ComponentAccessor.getCustomFieldManager()
                    .getCustomFieldObjectByName(RISK_IMPACT_FIELD_NAME);
            logger.info("Updating the Risk Impact value.");
            riskImpactCustomField.updateValue(null, issue, val, new DefaultIssueChangeHolder());
        } else {
            logger.warn("Issue is null.");
        }
        logger.debug("Workflow post-function done.");

    }

}
